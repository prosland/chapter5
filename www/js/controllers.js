angular.module('starter.controllers', ['e4101.config','starter.services', 'LocalStorageModule'])

.controller('IntroCtrl', function($scope, $state) {


/*
  ionic.Platform.ready(function() {
    // hide the status bar using the StatusBar plugin
    StatusBar.hide();
  });*/
  // Called to navigate to the main app
  var startApp = function() {
    $state.go('tab.dash');

    // Set a flag that we finished the tutorial
    window.localStorage['intro_done'] = true;
  };

  $scope.goMain = function(){
    startApp();
  }

  //No this is silly
  // Check if the user already did the tutorial and skip it if so
if(window.localStorage['intro_done'] === "true") {
    console.log('Skip intro');
    startApp();
  }
  else{
/*    setTimeout(function () {
      navigator.splashscreen&&navigator.splashscreen.hide();
    }, 750);*/
  }
  

  // Move to the next slide
  $scope.next = function() {
    $scope.$broadcast('slideBox.nextSlide');
  };

  // Our initial right buttons
  var rightButtons = [
    {
      content: 'Next',
      type: 'button-positive button-clear',
      tap: function(e) {
        // Go to the next slide on tap
        $scope.next();
      }
    }
  ];
  
  // Our initial left buttons
  var leftButtons = [
    {
      content: 'Skip',
      type: 'button-positive button-clear',
      tap: function(e) {
        // Start the app on tap
        startApp();
      }
    }
  ];

  // Bind the left and right buttons to the scope
  $scope.leftButtons = leftButtons;
  $scope.rightButtons = rightButtons;

  $scope.slideIndex = 0;
  // Called each time the slide changes
  $scope.slideChanged = function(index) {

    $scope.slideIndex = index;

    // Check if we should update the left buttons
    if(index > 0) {
      // If this is not the first slide, give it a back button
      $scope.leftButtons = [
        {
          content: 'Back',
          type: 'button-positive button-clear',
          tap: function(e) {
            // Move to the previous slide
            $scope.$broadcast('slideBox.prevSlide');
          }
        }
      ];
    } else {
      // This is the first slide, use the default left buttons
      $scope.leftButtons = leftButtons;
    }
    
    // If this is the last slide, set the right button to
    // move to the app
    if(index == 2) {
      $scope.rightButtons = [
        {
          content: 'Start using MyApp',
          type: 'button-positive button-clear',
          tap: function(e) {
            startApp();
          }
        }
      ];
    } else {
      // Otherwise, use the default buttons
      $scope.rightButtons = rightButtons;
    }
  };
})


.controller('DashCtrl', function( $scope, CFG, getData , localStorageService, $timeout ) {
  /*用于分类选择栏*/
  $scope.types = CFG.newsTypes;
  /*为使用 cordova inappbrowser 做准备*/
  $scope.openUrl = function(url,withLocation){
    withLocation = withLocation?'location=yes':'location=no';
    /*
    _system : 系统默认的浏览器中打开;
    _blank : InAppBrowser 中打开;
    _self : Cordova Web View中打开;
     */
    window.open(encodeURI(url),'_blank',withLocation);
  }
  /*用于刷新数据*/
  $scope.refreshData = function(type,isForceRefresh=false){
  /*判断缓存中是否有此分类下的新闻列表的数据,如果没有,就在线获取,如果有,就返回缓存*/
    if(isForceRefresh||!localStorageService.get('items_'+type )){

      getData.get( type ).then(function(data){
        $scope.items = data;
        console.log(' from remote api ')
        /*如果设备支持,就将返回的数据放进缓存*/
        if(localStorageService.isSupported) {
          localStorageService.set('items_'+type, $scope.items);
        }
      })
     .catch(function(response){
        console.log(response.status);
      }); 
    }else{
      console.log(' from cached ');
      /*从缓存中获取的数据*/
      $scope.items = localStorageService.get('items_'+type);
    }
  }
  /*
  判断缓存中是否有最后访问的新闻分类,如果有就是用它,如果没有,就使用第一个新闻分类。
  这样处理相当于记住最后一次访问的分类。
   */
  if(!localStorageService.get( 'lastViewCategory' )){
    /*定义默认显示的新闻分类*/
    $scope.type = $scope.types[0].id;
  }else{
    $scope.type = localStorageService.get( 'lastViewCategory' );
  }

  /*获取默认新闻分类下的新闻*/
  $scope.refreshData($scope.type);
  /*分类选择栏有变动时,刷新新闻*/
  $scope.update = function(type){
    $scope.type = type;
    /*如果设备支持,就将这个新闻分类放进缓存*/
    if(localStorageService.isSupported) {
      localStorageService.set('lastViewCategory', $scope.type );
    }
    $scope.refreshData($scope.type);
  }


  /*下拉刷新*/

  $scope.doRefresh = function(){
    console.log('Refreshing!');
    $timeout( function() {
        var type = localStorageService.get( 'lastViewCategory' ) || $scope.types[0].id;
         $scope.refreshData(type,true);
      $scope.$broadcast('scroll.refreshComplete');
       $scope.$apply()
    }, 1000);
  }

})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
