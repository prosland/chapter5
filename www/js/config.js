/*
* @Author: suifengtec
* @Date:   2017-05-28 10:30:37
* @Last Modified by:   suifengtec
* @Last Modified time: 2017-05-28 17:55:58
*/
angular.module('e4101.config',['LocalStorageModule'])
/*定义常量*/
.constant("CFG", {
  "baseURI": 'http://toutiao-ali.juheapi.com/toutiao/index?type=',
  "AppCode": 'ddea701210ca47d1b063dba737b44d4b',
  "AppSecret": '850695cf94627f43c7d5c41c83a58851',
  "AuthorizationHeader": 'Authorization:APPCODE ddea701210ca47d1b063dba737b44d4b',
  "newsTypes": [
  	{id:"top",name:"头条"},
    {id:"keji",name:"科技"},
    {id:"caijing",name:"财经"},
    {id:"guoji",name:"国际"},
    {id:"yule",name:"娱乐"},
    {id:"shishang",name:"时尚"},
    {id:"shehui",name:"社会"},
    {id:"guonei",name:"国内"}
  ]
})

/*对第三方缓存组件进行配置*/
.config(function (localStorageServiceProvider) {
  localStorageServiceProvider.setPrefix('e4101APP');
})
